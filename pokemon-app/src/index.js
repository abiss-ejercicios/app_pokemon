import React from 'react';
import ReactDOM from 'react-dom';

// Components
import PokemonApp from './PokemonApp';

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <PokemonApp />
  </React.StrictMode>,
  document.getElementById('root')
);
