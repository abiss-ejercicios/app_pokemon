import { useState, useEffect } from "react";
import { getPokemons } from './../helpers/getPokemon';


export const useFetchPokemon = (category) => {

     const [state, setState] = useState({
         data: [],
         loading: true
     });

     useEffect(() => {

        getPokemons( category )
            .then( pokemons => {

                setState({
                    data: pokemons,
                    loading: false
                });

            });
        
    }, [ category ]);

     return state;
};