import { environment } from "../environments/environment";

export const getPokemons = async ( category ) => {

    const url = `${environment.apiPokemon}/pokemons?q=${ encodeURI( category ) }&size=${environment.pagination.size}&page=${0}`
    const resp = await fetch(url);
    const { pokemons: { results } } = await resp.json();

    const pokemons = results.map(p => {

        return {
            id: p.id,
            title: p.name,
            url: p.url
        };
    });

    return pokemons;
};