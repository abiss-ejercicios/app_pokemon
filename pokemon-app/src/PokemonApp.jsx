import { useState } from 'react';

// Components
import { AddCategory, PokemonGrid } from './components';

import './PokemonApp.css';

const PokemonApp = () => {

    const [categories, setCategory] = useState([]);

    return (
        <div className='container-fluid'>
            <h2 className='mt-5 mb-5'>Pokemon App</h2>
            <AddCategory setCategory={setCategory} />
            <hr />
            {
                categories.length > 0 &&
                <h2>Resultados de la búsqueda</h2>
            }
            <ol style={{height: categories.length === 0 ? '55vh' : ''}}>
                {
                    categories.map(category => (
                        <PokemonGrid
                            key={category}
                            category={category}
                        />
                    ))
                }
            </ol>

            <hr />
            <div className="card-footer text-muted">

                <div className="row">
                    <div className="col-6">
                        Hecho por Andrés Biss
                    </div>
                    <div className="col-6">
                        <a href="https://ahbiss@bitbucket.org/abiss-ejercicios/app_pokemon.git" className="btn btn-primary" target="_blank" style={{ marginRight: '15px' }}>Link a repo app</a>
                        <a href="https://ahbiss@bitbucket.org/abiss-ejercicios/api_pokemon.git" className="btn btn-secondary" target="_blank">Link a repo api</a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PokemonApp;
