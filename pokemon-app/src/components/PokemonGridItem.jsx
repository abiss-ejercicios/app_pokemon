import React from 'react';

export const PokemonGridItem = ({ title, url }) => {

    const style = {
        width: '18rem',
    };

    return (
        <>
            <div className="card mt-5 animate__animated animate__fadeIn" style={style}>
                <img src={url} alt={title} />
                <div className="card-body">
                    <h5 className="card-title">{title}</h5>              
                </div>
            </div>
        </>
    )
}
