import React from 'react';

// Components
import { PokemonGridItem } from './PokemonGridItem';

// Hooks
import { useFetchPokemon } from '../hooks/useFetchPokemon';

export const PokemonGrid = ({ category }) => {

    const { data: images, loading } = useFetchPokemon( category ); 

    return (
        <>
            <span  className="badge bg-primary animate__animated animate__fadeIn">{category}</span >

            { loading && <p className="animate__animated animate__flash">Loading</p> }

            <div>
                {
                    images.map(img => (

                        <PokemonGridItem
                            key={img.id}
                            {...img}
                        />
                    ))
                }
            </div>
        </>
    )
}
