import React, { useState } from 'react';

export const AddCategory = ({ setCategory }) => {
  
    
    const [inputValue, setInputValue] = useState('');

    const handleInputChange = (e) => {

        setInputValue(e.target.value);
    };

    const handleSubmit = (e) => {

        e.preventDefault();

        if ( inputValue.trim().length > 2 ) {

            setCategory( categories => [ inputValue, ...categories ]);
            setInputValue('');
        }
    };

    const handleReset = () => {

        setCategory([]);
    }

    return (
        <form onSubmit={ handleSubmit }>
            <div class="input-group mb-3">
                <input
                    type="text"
                    value={inputValue}
                    onChange={handleInputChange}
                    className="form-control"
                    placeholder='Ingrese el nombre a buscar'
                />
                <input type="submit" value="Buscar" className="btn btn-outline-secondary"/>
                <button value="Limpiar" className="btn btn-outline-secondary" onClick={handleReset}>
                    Limpiar
                </button>
            </div>

        </form>
    )
}
