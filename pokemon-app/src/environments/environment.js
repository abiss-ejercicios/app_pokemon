
export const environment = {

    apiPokemon: process.env.REACT_APP_API_POKEMON,
    pagination: {
        size: process.env.REACT_APP_PAGINATION_SIZE
    }
};