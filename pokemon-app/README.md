# PokemonApp

1. Ejecutar ```npm install``` para construir los módulos de Node.

2. Asegurarse de contar con estas varaibles de entorno en los archivos ```.env.development``` y .```env.production``` correspondientes a los ambientes 

REACT_APP_API_POKEMON=http://localhost:8080/api
REACT_APP_PAGINATION_SIZE=5

3. Levantar la app con ```npm start```